package course.examples.examenu1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Distancia extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperatura);

        Intent intent = getIntent();
        double millas = Double.parseDouble(intent.getStringExtra(MainActivity.MESSAGE));
        double km = millas /0.62137 ;

        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText(millas + " millas = "+ km + " km" );

        setContentView(textView);
    }
}
