package course.examples.examenu1;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;
import android.view.Gravity;


public class MainActivity extends AppCompatActivity {
    public final static String MESSAGE = " ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabHost contenedorPestaña = (TabHost)findViewById(R.id.tabHost);
        contenedorPestaña.setup();

        //Tab 1
        TabHost.TabSpec pestaña = contenedorPestaña.newTabSpec("Tab One");
        pestaña.setContent(R.id.tab1);
        //spec.setIndicator("Grados");
        //pestaña.setIndicator("Pestaña 1", getResources().getDrawable(android.R.drawable.ic_menu_help));
        pestaña.setIndicator("", getResources().getDrawable(R.drawable.ic_grados));
        contenedorPestaña.addTab(pestaña);


        //Tab 2
        pestaña = contenedorPestaña.newTabSpec("Tab Two");
        pestaña.setContent(R.id.tab2);
        //spec.setIndicator("Area");
        pestaña.setIndicator("", getResources().getDrawable(R.drawable.ic_area));
        contenedorPestaña.addTab(pestaña);

        //Tab 3
        pestaña = contenedorPestaña.newTabSpec("Tab Three");
        pestaña.setContent(R.id.tab3);
        pestaña.setIndicator("", getResources().getDrawable(R.drawable.ic_distancia));
        contenedorPestaña.addTab(pestaña);

        //host.setCurrentTab(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return true;
    }


    public void enviarGrados(View view) {
        EditText editText = (EditText) findViewById(R.id.grados);
        if(editText.getText().toString().isEmpty()){
            Toast toast = Toast.makeText(this, "Introduce el valor", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }else {
            Intent intent = new Intent(this, Temperatura.class);
            String grados = editText.getText().toString();
            intent.putExtra(MESSAGE, grados);
            startActivity(intent);
        }

    }

    public void enviarLado(View view) {
        EditText editText = (EditText) findViewById(R.id.lado);
        if(editText.getText().toString().isEmpty()){
            Toast toast = Toast.makeText(this, "Introduce el valor", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }else {
            Intent intent = new Intent(this, Area.class);
            String lado = editText.getText().toString();
            intent.putExtra(MESSAGE, lado);
            startActivity(intent);
        }
    }

    public void enviarDistancia(View view) {
        EditText editText = (EditText) findViewById(R.id.distancia);

        if(editText.getText().toString().isEmpty()){
            Toast toast = Toast.makeText(this, "Introduce el valor", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.show();
        }else {
            Intent intent = new Intent(this, Distancia.class);
            String distancia = editText.getText().toString();
            intent.putExtra(MESSAGE, distancia);
            startActivity(intent);
        }

    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}