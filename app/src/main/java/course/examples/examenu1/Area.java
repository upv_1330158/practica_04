package course.examples.examenu1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Area extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperatura);

        Intent intent = getIntent();
        double lado = Double.parseDouble(intent.getStringExtra(MainActivity.MESSAGE));
        double resultado =(lado * lado);

        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText("Area: "+resultado);

        setContentView(textView);
    }
}
