package course.examples.examenu1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Temperatura extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperatura);

        Intent intent = getIntent();
        double faren = Double.parseDouble(intent.getStringExtra(MainActivity.MESSAGE));
        double cel = (faren - 32)/1.8000 ;

        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText( cel +" °C");

        setContentView(textView);
    }
}
